//https://jsonplaceholder.typicode.com/todos

	// solution Get method fetch request(retrieve todolist)

		fetch("https://jsonplaceholder.typicode.com/todos", {method: "GET"})

		.then(response => response.json())
		.then(result => {
			let title = result.map(element => element.title)
		    console.log(title);
		})



//  Get method fetch request (single todolist)

		fetch("https://jsonplaceholder.typicode.com/todos/1", {method: "GET"})

		.then(response => response.json())
		.then(result => console.log(result));


//  fetch POST

		fetch("https://jsonplaceholder.typicode.com/todos/", {method:"POST",
        headers:{
            "Content-Type" : "application/json"
        },
        body:JSON.stringify({
            title:"Hugot",
            completed:"true"
        })
		})
		.then(response => response.json())
		.then(result => console.log(result));



//  PUT method
		fetch("https://jsonplaceholder.typicode.com/todos/1", {method:"PUT",
        headers:{
            "Content-Type" : "application/json"
        },
        body:JSON.stringify({
            title:"fetch PUT",
            completed:"true"
        })
		})
		.then(response => response.json())
		.then(result => console.log(result));

	

//  PATCH method

		fetch("https://jsonplaceholder.typicode.com/todos/9",{method:"PATCH",
        headers:{
            "Content-Type" : "application/json"
        },
        body:JSON.stringify({
            title:"John Wick",
            description:"The Legend, The Myth",
            completed: "false",
            dateCompleted:"08/22/2020",
            userId:"1"
        })
		})
		.then(response => response.json())
		.then(result => console.log(result));


// add a date PATCH method

		fetch("https://jsonplaceholder.typicode.com/todos/9",
			{method:"PATCH",
        	headers:{
            "Content-Type" : "application/json"
        },
        body:JSON.stringify({
            completed:"true",
            dateCompleted:"01/31/2023"
        })
		})
		.then(response => response.json())
		.then(result => console.log(result));


//  DELETE   
		fetch("https://jsonplaceholder.typicode.com/todos/55", {
		method: "DELETE"
		})
		.then(res => res.json())
		.then(result => console.log(result));


